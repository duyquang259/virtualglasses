import VirtualGlasses from "./../models/virtualGlasses.js";
import { callApi } from "./callApi.js";
const getEle = (id) => document.getElementById(id);

document.getElementsByClassName("modal-title")[0].innerHTML =
  "Add Virtual Glasses";
getEle("btnCapNhat").style.display = "none";
getEle("btnReset").style.display = "block";
//Hàm reset form
const resetGlasses = () => {
  getEle("glassesForm").reset();
};
window.resetGlasses = resetGlasses;
//Lấy data từ Server
const renderGlassesList = (arr) => {
  let contentTR = "";
  arr.forEach((item) => {
    contentTR += `
        <div class="col-4">
                  <button class="btn btn-outline-secondary" type="submit" style = "border: none" onclick="getInfoItems(${item.id})" ;>
                      <img src="${item.src}" height= "100px" width="100px";>
                  </button>
                  <button data-toggle ="modal" data-target="#exampleModal" onclick ="editPd(${item.id})" class="btn btn-info">EDIT</button>
                  <button class="btn btn-danger" onclick="deletePd(${item.id})">DEL</button>
        </div>
        <br>
      `;
  });
  getEle("vglassesList").innerHTML = contentTR;
};
//HàmHiển thị sản phẩm
const showListProduct = () => {
  //bật loader
  getEle("loader").style.display = "block";
  callApi(`VirtualGlasses`, "GET", null)
    .then((result) => {
      //tắt loader
      getEle("loader").style.display = "none";
      renderGlassesList(result.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
showListProduct();

//Hiển thị Glassmodel & info
const handledWearGlassesModel = (item) => {
  let contentGM = "";
  contentGM = `
      <img src="${item.virtualImg}">
    `;
  let contentInfo = "";
  contentInfo = `
        <h4>${item.name} - ${item.brand} (${item.color})</h4>
        <div>
            <p>
                <span id ="price">$${item.price}</span>
                <span id="status">${
                  item.status === "0" ? "Out of stocking" : "Stocking"
                }</span>
            </p>
            <p> ${item.description}
            </p>
         </div>
    `;
  getEle("avatar").innerHTML = contentGM;
  getEle("glassesInfo").innerHTML = contentInfo;
  getEle("glassesInfo").style.display = "block";
};

// Hàm get Thông tin từng sản phẩm
const getInfoItems = (id) => {
  callApi(`VirtualGlasses/${id}`, "GET", null)
    .then((result) => {
      console.log(result.data);
      handledWearGlassesModel(result.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.getInfoItems = getInfoItems;

//FindIndex
const getIndex = (id) => {
  callApi(`VirtualGlasses`, "GET", null)
    .then((result) => {
      let index = result.data.findIndex((sp) => sp.id === id);
      console.log(index);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.getIndex = getIndex;

//Hàm getInfo từ user
const getInfo = () => {
  const _id = getEle("glassesID").value;
  const _src = getEle("glassesSrc").value;
  const _virtualImg = getEle("virtualImg").value;
  const _brand = getEle("glassesBrand").value;
  const _name = getEle("glassesName").value;
  const _color = getEle("glassesColor").value;
  const _price = getEle("glassesPrice").value;
  const _status = getEle("glassesStatus").value;
  const _description = getEle("glassDesc").value;

  //tạo đối tượng glass từ lớp đối tượng VirtualGlasses
  const glasses = new VirtualGlasses(
    _id,
    _src,
    _virtualImg,
    _brand,
    _name,
    _color,
    _price,
    _status,
    _description
  );
  return glasses;
};

//Thêm Kiếng
getEle("btnThemKieng").addEventListener("click", (event) => {
  //Enter
  event.preventDefault();
  //Dom tới từng ô input lấy value từ user nhập vào;
  const glasses = getInfo();
  // listGlasses.addGlasses(glasses);
  callApi(`VirtualGlasses`, "POST", glasses)
    .then(() => {
      showListProduct();
    })
    .catch((err) => {
      console.log(err);
    });
});

//Delete Kiếng
const deletePd = (id) => {
  //bật loader
  getEle("loader").style.display = "block";
  callApi(`VirtualGlasses/${id}`, "DELETE", null)
    .then(() => {
      //tắt loader
      getEle("loader").style.display = "none";
      showListProduct();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.deletePd = deletePd;

//Sửa Info Kiếng
const editPd = (id) => {
  document.getElementsByClassName("modal-title")[0].innerHTML =
    "Edit Virtual Glasses";
  //Mở lại nút cập nhật
  getEle("btnCapNhat").style.display = "inline-block";
  //Ẩn nút Thêm
  getEle("btnThemKieng").style.display = "none";
  console.log(id);
  //Lấy giá trị đã sửa từ user
  callApi(`VirtualGlasses/${id}`, "GET", null)
    .then((result) => {
      getEle("glassesID").value = result.data.id;
      getEle("glassesSrc").value = result.data.src;
      getEle("virtualImg").value = result.data.virtualImg;
      getEle("glassesBrand").value = result.data.brand;
      getEle("glassesName").value = result.data.name;
      getEle("glassesColor").value = result.data.color;
      getEle("glassesPrice").value = result.data.price;
      getEle("glassesStatus").value = result.data.status;
      getEle("glassDesc").value = result.data.description;
    })
    .catch((err) => {
      console.log(err);
    });
};
window.editPd = editPd;

//Cập nhật
getEle("btnCapNhat").addEventListener("click", () => {
  const glasses = getInfo();
  //bật loader
  getEle("loader").style.display = "block";
  callApi(`VirtualGlasses/${glasses.id}`, "PUT", glasses)
    .then(() => {
      //tắt loader
      getEle("loader").style.display = "none";
      showListProduct();
    })
    .catch((err) => {
      console.log(err);
    });
});
