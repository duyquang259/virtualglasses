//Bước 1: Đối tượng virtualGlasses trong models/virtualGlasses.js
//Bước 3: Các thuộc tính của lớp đối tượng
class VirtualGlasses {
  constructor(
    _id,
    _src,
    _virtualImg,
    _brand,
    _name,
    _color,
    _price,
    _status,
    _description
  ) {
    this.id = _id;
    this.src = _src;
    this.virtualImg = _virtualImg;
    this.brand = _brand;
    this.name = _name;
    this.color = _color;
    this.price = _price;
    this.status = _status;
    this.description = _description;
  }
  //Các Phương thức
}
export default VirtualGlasses;
